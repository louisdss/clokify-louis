package com.example.clockfy_firstproject.uicustom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.clockfy_firstproject.R;

public class ToolbarCustom extends ConstraintLayout {

    private ImageView imageView;
    private TextView textView;

    public ToolbarCustom(@NonNull Context context) {
        super(context);
        init(context, null, 0);
    }

    public ToolbarCustom(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public ToolbarCustom(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    public ToolbarCustom(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        inflate(context, R.layout.toolbar_back, this);

        imageView = findViewById(R.id.back);
        textView = findViewById(R.id.detailtoolbar);

        if (attrs != null) {
            TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ToolbarCustom);
            try {
                textView.setText(attributes.getString(R.styleable.ToolbarCustom_text));
            } finally {
                attributes.recycle();
            }
        }

    }

    public void setToolbarText(String text) {
        if (text != null)
            textView.setText(text);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        setOnBackClikListener(l);
    }

    public void setOnBackClikListener(OnClickListener clikListener) {
        if (clikListener != null)
            imageView.setOnClickListener(clikListener);
    }
}
