package com.example.clockfy_firstproject.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clockfy_firstproject.Activity.DetailActivityUser;
import com.example.clockfy_firstproject.R;
import com.example.clockfy_firstproject.model.ActivityResult;
import com.example.clockfy_firstproject.util.ConstantManager;
import com.example.clockfy_firstproject.util.InterfaceManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class RecordActivityAdapter extends RecyclerView.Adapter<RecordActivityAdapter.ViewHolder> {

    private ArrayList<ActivityResult> activityResults = new ArrayList<>();
    private Context context;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyy", Locale.getDefault());
    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    private String startTimeDetail = "00:00";
    private String endTimeDetail = "00:00";


    public RecordActivityAdapter(Context context) {
        this.context = context;
    }

    public void updateDataAdapter(ArrayList<ActivityResult> model) {
        this.activityResults = model;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_activity,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ActivityResult result = activityResults.get(position);
        String startTime = "00:00";
        String endTime = "00:00";
        String dateNext;
        String activity = result.activity != null ? result.activity : "";
        int idModel = result.id;
        String dateFirst = dateFormat.format(InterfaceManager.getInstance().stringDateFormatter(result.createdAt));
        if (result.start_time != null) {
            startTime = timeFormat.format(InterfaceManager.getInstance().stringDateFormatter(result.start_time));
            startTimeDetail = startTime;
            holder.recordTimer.setText(startTime);
        }
        if (result.end_time != null) {
            endTime = timeFormat.format(InterfaceManager.getInstance().stringDateFormatter(result.end_time));
            endTimeDetail = endTime;
        }
        holder.sectionNameTextview.setText(dateFirst);
        holder.startEndTimer.setText(startTime + " - " + endTime);
        holder.description.setText(activity);

        if (position > 0) {
            dateNext = dateFormat.format(InterfaceManager.getInstance().stringDateFormatter(activityResults.get(position - 1).createdAt));
            if (dateFirst.compareTo(dateNext) == 0) {
                holder.sectionNameTextview.setVisibility(View.GONE);
            } else {
                holder.sectionNameTextview.setVisibility(View.VISIBLE);
            }
        }


        holder.celllConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivityUser.class);
                intent.putExtra(ConstantManager.IDDETAIL,idModel);
                context.startActivity(intent);

            }
        });



    }

    @Override
    public int getItemCount() {
        return activityResults.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView sectionNameTextview;
        TextView recordTimer;
        TextView startEndTimer;
        TextView description;
        ConstraintLayout celllConstraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            sectionNameTextview = itemView.findViewById(R.id.section_title);
            recordTimer = itemView.findViewById(R.id.record_timer_textview);
            startEndTimer = itemView.findViewById(R.id.start_end_timer_textview);
            description = itemView.findViewById(R.id.keterangan_textview);
            celllConstraintLayout = itemView.findViewById(R.id.cellView);
        }
    }
}
