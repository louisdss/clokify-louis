package com.example.clockfy_firstproject.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.clockfy_firstproject.R;
import com.example.clockfy_firstproject.service.ClockifyService;
import com.example.clockfy_firstproject.service.auth.ActivityService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimerFragment extends Fragment {
    private Chronometer chronometer;
    private TextView startTimeNow, finishTimeNow, dateTimeNow, dateFinishNow;
    private Context context;
    private Button buttonStartTime;
    private Button buttonStopTime;
    private Button buttonDeleteTime;
    private EditText activityText;

    private boolean running;
    String currentTime;
    String currentDate;
    private long pauseOffset;
    private String startTimeInput;
    private String endTimeInput;

    private static SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = ((ViewGroup) inflater.inflate(R.layout.fragment_timer, container, false));
        context = rootView.getContext();

        initUI(rootView);
        initLayout();
        initEvent();

        return rootView;
    }

    private void initUI(ViewGroup rootView) {
        chronometer = rootView.findViewById(R.id.timer_tv);
        startTimeNow = rootView.findViewById(R.id.time_now_after_start_tv);
        finishTimeNow = rootView.findViewById(R.id.time_now_after_finish_tv);
        dateTimeNow = rootView.findViewById(R.id.date_now_after_start_tv);
        dateFinishNow = rootView.findViewById(R.id.date_now_after_finish_tv);
        buttonStartTime = rootView.findViewById(R.id.start_button_homeactivity);
        buttonStopTime = rootView.findViewById(R.id.stop_button_homeActivity);
        buttonDeleteTime = rootView.findViewById(R.id.delete_button_homeActivity);
        activityText = rootView.findViewById(R.id.activityhere_edittext);
    }

    private void initLayout() {
        buttonStopTime.setVisibility(View.GONE);
        buttonDeleteTime.setVisibility(View.GONE);

        chronometer.setFormat("00:00:00");
    }

    private void initEvent() {
        chronometer.setOnChronometerTickListener(chronometer -> initTimerChronometer());

        buttonStartTime.setOnClickListener(v -> {
            currentTime = timeFormat.format(new Date());
            currentDate = dateFormat.format(new Date());
            if (!running) {
                buttonStartTime.setVisibility(View.GONE);
                buttonStopTime.setVisibility(View.VISIBLE);
                buttonDeleteTime.setVisibility(View.VISIBLE);

                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                running = true;

                dateTimeNow.setText(currentDate);
                startTimeNow.setText(currentTime);

                buttonStopTime.setText("Stop");
                if (dateFinishNow.getText().toString().equalsIgnoreCase("-")) {

                } else {
                    dateFinishNow.setText("-");
                    finishTimeNow.setText("-");
                }
            }

            startTimeInput = timeFormatter.format(new Date());
        });


        buttonStopTime.setOnClickListener(v -> {
            currentTime = timeFormat.format(new Date());
            currentDate = dateFormat.format(new Date());

            if (running) {
                running = false;

                chronometer.stop();
                pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();

                buttonStopTime.setText("Save");
                dateFinishNow.setText(currentDate);
                finishTimeNow.setText(currentTime);

            } else {
                //code POST TO API disini
                createActivities();
            }

            endTimeInput = timeFormatter.format(new Date());
        });


        buttonDeleteTime.setOnClickListener(v -> {
            currentTime = timeFormat.format(new Date());
            currentDate = dateFormat.format(new Date());

            if (!running) {
                buttonStartTime.setVisibility(View.VISIBLE);
                buttonStopTime.setVisibility(View.GONE);
                buttonDeleteTime.setVisibility(View.GONE);
                dateFinishNow.setText("-");
                finishTimeNow.setText("-");
                dateTimeNow.setText("-");
                startTimeNow.setText("-");
                chronometer.setBase(SystemClock.elapsedRealtime());
            } else {
                Toast.makeText(context, "Tekan save dulu cok", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initTimerChronometer() {
        long time = SystemClock.elapsedRealtime() - chronometer.getBase();
        int h = (int) (time / 3600000);
        int m = (int) (time - h * 3600000) / 60000;
        int s = (int) (time - h * 3600000 - m * 60000) / 1000;
        String hh = h < 10 ? "0" + h : h + "";
        String mm = m < 10 ? "0" + m : m + "";
        String ss = s < 10 ? "0" + s : s + "";
        chronometer.setText(hh + ":" + mm + ":" + ss);
    }

    public void createActivities() {
        ActivityService createActivitiesService = ClockifyService.create(ActivityService.class);
        createActivitiesService.createActivities(startTimeInput, endTimeInput, dateTimeNow.getText().toString(), dateFinishNow.getText().toString(), chronometer.getText().toString(), activityText.getText().toString(), "").enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Coba tekan save lagi", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}