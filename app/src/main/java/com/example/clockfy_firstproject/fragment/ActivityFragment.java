package com.example.clockfy_firstproject.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.clockfy_firstproject.R;
import com.example.clockfy_firstproject.adapter.RecordActivityAdapter;
import com.example.clockfy_firstproject.model.ActivityListModel;
import com.example.clockfy_firstproject.model.ActivityResult;
import com.example.clockfy_firstproject.service.ClockifyService;
import com.example.clockfy_firstproject.service.DeleteRespone;
import com.example.clockfy_firstproject.service.auth.ActivityService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityFragment extends Fragment {

    private ArrayList<ActivityResult> modelByJSON = new ArrayList<>();

    private Context context;
    private RecordActivityAdapter recordActivityAdapter;
    private RecyclerView mainRecycleView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = ((ViewGroup) inflater.inflate(R.layout.fragment_activity , container, false));
        context = rootView.getContext();

        mainRecycleView = rootView.findViewById(R.id.recycleView_Activity);

        initLayout();
        getAllActivities();
        swipedelete();


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllActivities();
    }

    private void swipedelete(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT  ) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                Toast.makeText(context, "on Move", Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                //Remove swiped item from list and notify the RecyclerView
                deteleActivities(viewHolder);

            }
        };


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mainRecycleView);
    }

    private void initLayout(){
        recordActivityAdapter = new RecordActivityAdapter(context);
        mainRecycleView.setLayoutManager(new LinearLayoutManager(context));
        mainRecycleView.setAdapter(recordActivityAdapter);
        mainRecycleView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
    }


    public void deteleActivities(RecyclerView.ViewHolder viewHolder){
        ActivityService deteleteService = ClockifyService.create(ActivityService.class);
        System.out.println(modelByJSON.get(viewHolder.getAdapterPosition()).id + " anjayy");
        deteleteService.deteleActivities(modelByJSON.get(viewHolder.getAdapterPosition()).id).enqueue(new Callback<DeleteRespone>() {
            @Override
            public void onResponse(Call<DeleteRespone> call, Response<DeleteRespone> response) {
                int position = viewHolder.getAdapterPosition();
                modelByJSON.remove(position);
                recordActivityAdapter.notifyDataSetChanged();
                Toast.makeText(context, response.body().message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<DeleteRespone> call, Throwable t) {
                Toast.makeText(context, "Gagal delete cuk", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getAllActivities(){
        ActivityService dataRecordActivityService = ClockifyService.create(ActivityService.class);
        dataRecordActivityService.getAllActivityList().enqueue(new Callback<ActivityListModel>() {
            @Override
            public void onResponse(Call<ActivityListModel> call, Response<ActivityListModel> response) {
                if (response.isSuccessful() && response.body() != null){
                    modelByJSON = response.body().results;
                    recordActivityAdapter.updateDataAdapter(modelByJSON);
                    Toast.makeText(context,"Success narik data", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context,"Gagal cok", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ActivityListModel> call, Throwable t) {
                Toast.makeText(context,"Gagal", Toast.LENGTH_SHORT).show();
            }
        });
    }
}