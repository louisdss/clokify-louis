package com.example.clockfy_firstproject.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.clockfy_firstproject.Activity.HomeActivity;
import com.example.clockfy_firstproject.Activity.LoginActivity;
import com.example.clockfy_firstproject.Activity.NewAccountActivity;
import com.example.clockfy_firstproject.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Handler handler = new Handler();
        handler.postDelayed(() -> {
            finish();

            startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
        }, 2000);
    }
}