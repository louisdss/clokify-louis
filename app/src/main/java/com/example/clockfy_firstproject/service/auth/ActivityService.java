package com.example.clockfy_firstproject.service.auth;

import com.example.clockfy_firstproject.model.ActivityListModel;
import com.example.clockfy_firstproject.model.ActivityResult;
import com.example.clockfy_firstproject.model.DetailActivitiesModel;
import com.example.clockfy_firstproject.service.AuthResponse;
import com.example.clockfy_firstproject.service.DeleteRespone;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ActivityService {
    @FormUrlEncoded
    @POST("timers")
    Call<Void> createActivities(@Field("start_time") String starttime,
                                @Field("end_time") String endtime,
                                @Field("start_date") String startdate,
                                @Field("end_date") String enddate,
                                @Field("duration") String duration,
                                @Field("activity") String activity,
                                @Field("location") String location);

    @GET("timers")
    Call<ActivityListModel> getAllActivityList();


    @DELETE("timers/{id}")
    Call<DeleteRespone> deteleActivities(@Path("id") int id);

    @GET("timers/{id}")
    Call<DetailActivitiesModel> detailActivities(@Path("id") int id);


    @FormUrlEncoded
    @PUT("timers/{id}")
    Call<Void> updateDATA(@Path("id") int id,
                          @Field("activity") String activity);

}
