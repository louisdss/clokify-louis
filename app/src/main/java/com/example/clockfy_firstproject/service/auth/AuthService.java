package com.example.clockfy_firstproject.service.auth;


import com.example.clockfy_firstproject.service.AuthResponse;
import com.example.clockfy_firstproject.service.SignUpRespone;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface AuthService {
    @FormUrlEncoded
    @POST("users/sessions")
    Call<AuthResponse> loginUser(@Field("email") String email,
                                 @Field("password") String password);

    @FormUrlEncoded
    @POST("users")
    Call<SignUpRespone> signup(@Field("email") String email,
                               @Field("nama") String nama,
                               @Field("password") String password);
}
