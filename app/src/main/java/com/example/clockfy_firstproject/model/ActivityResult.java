package com.example.clockfy_firstproject.model;

public class ActivityResult{
    public int id;
    public int user_id;
    public String start_time;
    public String end_time;
    public String activity;
    public String location;
    public String createdAt;
    public String updatedAt;
    public String deletedAt;
}