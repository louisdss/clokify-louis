package com.example.clockfy_firstproject.model;

public class User {
    private String email,pass;
    private int userID;

    public User(String email, String pass, int userID) {
        this.email = email;
        this.pass = pass;
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
