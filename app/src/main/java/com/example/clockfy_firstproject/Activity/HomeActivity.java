package com.example.clockfy_firstproject.Activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.clockfy_firstproject.R;
import com.example.clockfy_firstproject.fragment.ActivityFragment;
import com.example.clockfy_firstproject.fragment.TimerFragment;
import com.google.android.material.tabs.TabLayout;

public class HomeActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private DemoCollectionPagerAdapter adapter;
    private FragmentManager manager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        viewPager = findViewById(R.id.pager);
        TabLayout tabLayout = findViewById(R.id.tab_layout);

        adapter = new DemoCollectionPagerAdapter(manager);
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    public class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {
        public DemoCollectionPagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "TIMER";
            } else {
                return "ACTIVITY";
            }
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Fragment timerFragment = new TimerFragment();
            Fragment activityFragment = new ActivityFragment();

            Bundle argsTimerFragment = new Bundle();
            timerFragment.setArguments(argsTimerFragment);

            Bundle argsActivityFragment = new Bundle();
            activityFragment.setArguments(argsActivityFragment);

            if (position == 0) {
                return timerFragment;
            } else {
                return activityFragment;
            }
        }
    }
}