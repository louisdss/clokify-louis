package com.example.clockfy_firstproject.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clockfy_firstproject.R;
import com.example.clockfy_firstproject.service.ClockifyService;
import com.example.clockfy_firstproject.service.SignUpRespone;
import com.example.clockfy_firstproject.service.auth.AuthService;
import com.example.clockfy_firstproject.uicustom.ToolbarCustom;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewAccountActivity extends AppCompatActivity {
    private ToolbarCustom toolbarCustom;
    private Button creat_regis_button;
    private Dialog popup;
    private CardView background;
    private TextView emailRegis, creatPassRegis, confrimPassRegis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        initUI();
        initEvent();
    }

    private void initUI(){
        toolbarCustom = findViewById(R.id.toolbarbackregis);
        creat_regis_button = findViewById(R.id.create_button);
        emailRegis = findViewById(R.id.input_email_signin_regis);
        creatPassRegis = findViewById(R.id.create_password_regis);
        confrimPassRegis = findViewById(R.id.confirm_create_password_regis);
    }

    private void initEvent(){
        toolbarCustom.setOnBackClikListener(v -> onBackPressed());
        creat_regis_button.setOnClickListener(v -> {
            System.out.println(creatPassRegis.getText().toString()+" anjay");
            System.out.println(confrimPassRegis.getText().toString()+" anjaya");
            if(!emailValidator(emailRegis.getText().toString().trim())){
                Toast.makeText(NewAccountActivity.this, "Please insert valid email", Toast.LENGTH_SHORT).show();
            }else if(creatPassRegis.getText().toString().equalsIgnoreCase("")){
                Toast.makeText(NewAccountActivity.this, "Please Insert Password", Toast.LENGTH_SHORT).show();
            }else{
                if(confrimPassRegis.getText().toString().equals(creatPassRegis.getText().toString())){
                    register();
                }else {
                    Toast.makeText(NewAccountActivity.this, "Confirm pass doesnt macth", Toast.LENGTH_SHORT).show();
                }
            }


        });
    }

    private void initDialog() {
        popup = new AppCompatDialog(this);
        popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popup.setContentView(R.layout.popup_sucsses);
        Window window = popup.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        popup.setCanceledOnTouchOutside(true);

        background = popup.findViewById(R.id.backgroundsucces);
        background.setOnClickListener(m -> {
            if (popup.isShowing()) {
                startActivity(new Intent(NewAccountActivity.this, LoginActivity.class));
                popup.dismiss();
            }

        });
        popup.show();
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void register() {
        AuthService signUpService = ClockifyService.create(AuthService.class);
        signUpService.signup(emailRegis.getText().toString(), "", confrimPassRegis.getText().toString()).enqueue(new Callback<SignUpRespone>() {
            @Override
            public void onResponse(Call<SignUpRespone> call, Response<SignUpRespone> response) {
                if (response.isSuccessful() && response.body() != null) {

                    Toast.makeText(NewAccountActivity.this, response.body().message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(NewAccountActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(NewAccountActivity.this, response.body().message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignUpRespone> call, Throwable t) {
                Toast.makeText(NewAccountActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}