package com.example.clockfy_firstproject.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clockfy_firstproject.R;
import com.example.clockfy_firstproject.model.ActivityResult;
import com.example.clockfy_firstproject.model.DetailActivitiesModel;
import com.example.clockfy_firstproject.service.ClockifyService;
import com.example.clockfy_firstproject.service.DeleteRespone;
import com.example.clockfy_firstproject.service.auth.ActivityService;
import com.example.clockfy_firstproject.uicustom.ToolbarCustom;
import com.example.clockfy_firstproject.util.ConstantManager;
import com.example.clockfy_firstproject.util.InterfaceManager;

import java.text.SimpleDateFormat;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.clockfy_firstproject.adapter.RecordActivityAdapter.timeFormat;

public class DetailActivityUser extends AppCompatActivity {
    private ToolbarCustom toolbarCustom;
    private ActivityResult detailActivities;
    private int idModel;

    private String startTimeDetail = "00:00";
    private String endTimeDetail = "00:00";

    private ImageView backButton;
    private TextView detailToolbar;
    private TextView startTimeNow, finishTimeNow, dateTimeNow, dateFinishNow;
    private Button buttonSaveTime;
    private Button buttonDeleteTime;
    private EditText activityText;
    private Chronometer chronometer;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyy", Locale.getDefault());
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user);

        initData();
        initUI();
        getDetailActivities(idModel);


        backButton.setBackgroundResource(R.drawable.back_white);
        detailToolbar.setText("Detail");


        toolbarCustom.setOnBackClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        buttonSaveTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateData(idModel,activityText.getText().toString());
            }
        });

        buttonDeleteTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deteleActivities(idModel);
                Intent intent = new Intent(DetailActivityUser.this, HomeActivity.class);
                startActivity(intent);
            }
        });


    }

    public void initToXML(){
        String startTime = "00:00";
        String endTime = "00:00";
        String dateNext;
        String activity = detailActivities.activity != null ? detailActivities.activity : "";
        String dateFirst = dateFormat.format(InterfaceManager.getInstance().stringDateFormatter(detailActivities.createdAt));
        chronometer.setText(detailActivities.start_time);
        if (detailActivities.start_time != null) {
            startTime = timeFormat.format(InterfaceManager.getInstance().stringDateFormatter(detailActivities.start_time));

        }
        if (detailActivities.end_time != null) {
            endTime = timeFormat.format(InterfaceManager.getInstance().stringDateFormatter(detailActivities.end_time));
        }
        chronometer.setText(startTime);
        startTimeNow.setText(startTime);
        finishTimeNow.setText(endTime);
        dateTimeNow.setText(dateFirst);
        dateFinishNow.setText(dateFirst);
        activityText.setText(activity);
    }

    public void UpdateData(int id, String textActivity){
        ActivityService updateDataDetailService = ClockifyService.create(ActivityService.class);
        updateDataDetailService.updateDATA(id,textActivity).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(DetailActivityUser.this,"Update Activity Berhasil", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(DetailActivityUser.this,"Update Activity GAGAL", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getDetailActivities(int id){
        ActivityService detailActivityService = ClockifyService.create(ActivityService.class);
        detailActivityService.detailActivities(id).enqueue(new Callback<DetailActivitiesModel>() {
            @Override
            public void onResponse(Call<DetailActivitiesModel> call, Response<DetailActivitiesModel> response) {
                detailActivities = response.body().results;
                System.out.println(response.body().results.start_time  +" suksessss");
                initToXML();
                Toast.makeText(DetailActivityUser.this,"Success narik data", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<DetailActivitiesModel> call, Throwable t) {

            }
        });
    }

    public void deteleActivities(int id){
        ActivityService deteleteService = ClockifyService.create(ActivityService.class);
        deteleteService.deteleActivities(id).enqueue(new Callback<DeleteRespone>() {
            @Override
            public void onResponse(Call<DeleteRespone> call, Response<DeleteRespone> response) {
                Toast.makeText(DetailActivityUser.this, response.body().message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<DeleteRespone> call, Throwable t) {
                Toast.makeText(DetailActivityUser.this, "Gagal delete cuk", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initData() {
        idModel = getIntent().getIntExtra(ConstantManager.IDDETAIL,0);
        System.out.println(idModel+" asdasdasdddd");
    }

    private void initUI() {
        backButton = findViewById(R.id.back);
        detailToolbar = findViewById(R.id.detailtoolbar);
        toolbarCustom = findViewById(R.id.toolbarback_detail);

        chronometer = findViewById(R.id.timer_tv_detail);
        startTimeNow = findViewById(R.id.time_now_after_start_tv_detail);
        finishTimeNow = findViewById(R.id.time_now_after_finish_tv_detail);
        dateTimeNow = findViewById(R.id.date_now_after_start_tv_detail);
        dateFinishNow = findViewById(R.id.date_now_after_finish_tv_detail);
        buttonSaveTime = findViewById(R.id.save_button_detail_activity);
        buttonDeleteTime = findViewById(R.id.delete_button_detailActivity);
        activityText =  (EditText) findViewById(R.id.activityhere_edittext_detail);

    }
}