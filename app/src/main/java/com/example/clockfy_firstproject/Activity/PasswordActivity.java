package com.example.clockfy_firstproject.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.clockfy_firstproject.R;
import com.example.clockfy_firstproject.service.AuthResponse;
import com.example.clockfy_firstproject.service.ClockifyService;
import com.example.clockfy_firstproject.service.auth.AuthService;
import com.example.clockfy_firstproject.uicustom.ToolbarCustom;
import com.example.clockfy_firstproject.util.ConstantManager;
import com.example.clockfy_firstproject.util.UserDefaults;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordActivity extends AppCompatActivity {
    private ToolbarCustom toolbarCustom;
    private Button okButton;
    private TextInputEditText textInputPassword;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        initData();
        initUI();
        initEvent();
    }

    private void initUI() {
        toolbarCustom = findViewById(R.id.toolbarback);
        okButton = findViewById(R.id.ok_button);
        textInputPassword = findViewById(R.id.password_textinput);
    }

    private void initEvent() {
        okButton.setOnClickListener(v -> {
            if (textInputPassword.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(PasswordActivity.this, getResources().getString(R.string.errorfillmsg), Toast.LENGTH_SHORT).show();
            } else {
                login();
            }
        });

        toolbarCustom.setOnBackClikListener(v -> onBackPressed());
    }

    private void login() {
        AuthService service = ClockifyService.create(AuthService.class);
        service.loginUser(email, textInputPassword.getText().toString()).enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                if (response.isSuccessful() && response.body().token != null) {
                    String token = response.body().token;
                    UserDefaults.getInstance().setString(UserDefaults.TOKEN_KEY, "Bearer " + token);
                    Intent intent = new Intent(PasswordActivity.this, HomeActivity.class);

                    startActivity(intent);
                } else {
                    Toast.makeText(PasswordActivity.this, "Ada yang salah bung", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                Toast.makeText(PasswordActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initData() {
        email = getIntent().getStringExtra(ConstantManager.EMAIL);
    }
}