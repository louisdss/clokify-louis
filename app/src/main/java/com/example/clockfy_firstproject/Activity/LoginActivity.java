package com.example.clockfy_firstproject.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.clockfy_firstproject.R;
import com.example.clockfy_firstproject.util.ConstantManager;
import com.google.android.material.textfield.TextInputEditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    Button signin_button;
    TextView create_new_account;
    TextInputEditText textInputEditText_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginactivity);

        initUI();
        initEvent();
    }

    private void initUI() {
        signin_button = findViewById(R.id.buttonSingIn);
        create_new_account = findViewById(R.id.createNewAcountTextView);
        textInputEditText_email = findViewById(R.id.input_email_signin);
    }

    private void initEvent() {
        signin_button.setOnClickListener(v -> {
//            if (emailValidator(textInputEditText_email.getText().toString().trim())) {
            Intent intent = new Intent(LoginActivity.this, PasswordActivity.class);
            intent.putExtra(ConstantManager.EMAIL, textInputEditText_email.getText().toString());
            startActivity(intent);
//            } else {
//                Toast.makeText(LoginActivity.this, "Please insert valid email", Toast.LENGTH_SHORT).show();
//            }
        });

        create_new_account.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, NewAccountActivity.class)));
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}